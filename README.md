# json_color

It is a library to colorize JSON.

[![Gem Version](https://badge.fury.io/rb/json_color.png)](http://badge.fury.io/rb/json_color)
[![Build Status](https://drone.io/bitbucket.org/winebarrel/json_color/status.png)](https://drone.io/bitbucket.org/winebarrel/json_color/latest)

## Installation

Add this line to your application's Gemfile:

    gem 'json_color'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install json_color

## Usage

```ruby
require 'json_color'

puts JsonColor.colorize(<<-EOS)
{
    "glossary": {
        "title": "example glossary",
    "GlossDiv": {
            "title": "S",
      "GlossList": {
                "GlossEntry": {
                    "ID": "SGML",
          "SortAs": "SGML",
          "GlossTerm": "Standard Generalized Markup Language",
          "Acronym": "SGML",
          "Abbrev": "ISO 8879:1986",
          "GlossDef": {
                        "para": "A meta-markup language, used to create markup languages such as DocBook.",
            "GlossSeeAlso": ["GML", "XML"]
                    },
          "GlossSee": "markup"
                }
            }
        }
    }
}
EOS
```

![Screenshot](https://bitbucket.org/winebarrel/json_color/downloads/json_color-screenshot2.png)

# Default color map

```ruby
module JsonColor
  DEFAULT_COLOR_MAP = {
    :key    => :intense_blue,
    :string => :green,
    :null   => :intense_black,
  }
  ...
end
```
