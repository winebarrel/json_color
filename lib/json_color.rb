require 'strscan'
require 'term/ansicolor'

require 'json_color/version'
require 'json_color/parser.tab'

module JsonColor
  DEFAULT_COLOR_MAP = {
    :key    => :intense_blue,
    :string => :green,
    :null   => :intense_black,
  }

  def self.colorize(src, color_map = nil)
    JsonColor::Parser.parse(src, color_map || DEFAULT_COLOR_MAP)
  end
end
