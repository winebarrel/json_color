describe 'JsonColor' do
  let(:json) do
    <<-EOS
{"web-app": {
  "servlet": [
    {
      "servlet-name": "cofaxCDS",
      "servlet-class": "org.cofax.cds.CDSServlet",
      "init-param": {
        "configGlossary:installationAt": "Philadelphia, PA",
        "configGlossary:adminEmail": "ksm@pobox.com",
        "configGlossary:poweredBy": "Cofax",
        "configGlossary:poweredByIcon": "/images/cofax.gif",
        "configGlossary:staticPath": "/content/static",
        "templateProcessorClass": "org.cofax.WysiwygTemplate",
        "templateLoaderClass": "org.cofax.FilesTemplateLoader",
        "templatePath": "templates",
        "templateOverridePath": "",
        "defaultListTemplate": "listTemplate.htm",
        "defaultFileTemplate": "articleTemplate.htm",
        "useJSP": false,
        "jspListTemplate": "listTemplate.jsp",
        "jspFileTemplate": "articleTemplate.jsp",
        "cachePackageTagsTrack": 200,
        "cachePackageTagsStore": 200,
        "cachePackageTagsRefresh": 60,
        "cacheTemplatesTrack": 100,
        "cacheTemplatesStore": 50,
        "cacheTemplatesRefresh": 15,
        "cachePagesTrack": 200,
        "cachePagesStore": 100,
        "cachePagesRefresh": 10,
        "cachePagesDirtyRead": 10,
        "searchEngineListTemplate": "forSearchEnginesList.htm",
        "searchEngineFileTemplate": "forSearchEngines.htm",
        "searchEngineRobotsDb": "WEB-INF/robots.db",
        "useDataStore": true,
        "dataStoreClass": "org.cofax.SqlDataStore",
        "redirectionClass": "org.cofax.SqlRedirection",
        "dataStoreName": "cofax",
        "dataStoreDriver": "com.microsoft.jdbc.sqlserver.SQLServerDriver",
        "dataStoreUrl": "jdbc:microsoft:sqlserver://LOCALHOST:1433;DatabaseName=goon",
        "dataStoreUser": "sa",
        "dataStorePassword": "dataStoreTestQuery",
        "dataStoreTestQuery": "SET NOCOUNT ON;select test='test';",
        "dataStoreLogFile": "/usr/local/tomcat/logs/datastore.log",
        "dataStoreInitConns": 10,
        "dataStoreMaxConns": 100,
        "dataStoreConnUsageLimit": 100,
        "dataStoreLogLevel": "debug",
        "maxUrlLength": 500,
        "log": null}},
    {
      "servlet-name": "cofaxEmail",
      "servlet-class": "org.cofax.cds.EmailServlet",
      "init-param": {
      "mailHost": "mail1",
      "mailHostOverride": "mail2"}},
    {
      "servlet-name": "cofaxAdmin",
      "servlet-class": "org.cofax.cds.AdminServlet"},

    {
      "servlet-name": "fileServlet",
      "servlet-class": "org.cofax.cds.FileServlet"},
    {
      "servlet-name": "cofaxTools",
      "servlet-class": "org.cofax.cms.CofaxToolsServlet",
      "init-param": {
        "templatePath": "toolstemplates/",
        "log": 1,
        "logLocation": "/usr/local/tomcat/logs/CofaxTools.log",
        "logMaxSize": "",
        "dataLog": 1,
        "dataLogLocation": "/usr/local/tomcat/logs/dataLog.log",
        "dataLogMaxSize": "",
        "removePageCache": "/content/admin/remove?cache=pages&id=",
        "removeTemplateCache": "/content/admin/remove?cache=templates&id=",
        "fileTransferFolder": "/usr/local/tomcat/webapps/content/fileTransferFolder",
        "lookInContext": 1,
        "adminGroupID": 4,
        "betaServer": true}}],
  "servlet-mapping": {
    "cofaxCDS": "/",
    "cofaxEmail": "/cofaxutil/aemail/*",
    "cofaxAdmin": "/admin/*",
    "fileServlet": "/static/*",
    "cofaxTools": "/tools/*"},

  "taglib": {
    "taglib-uri": "cofax.tld",
    "taglib-location": "/WEB-INF/tlds/cofax.tld"}}}
    EOS
  end

  it 'colorize JSON' do
    colorized = JsonColor.colorize(json)

    expect(colorized).to eq(<<-EOS)
{\e[94m"web-app"\e[0m: {
  \e[94m"servlet"\e[0m: [
    {
      \e[94m"servlet-name"\e[0m: \e[32m"cofaxCDS"\e[0m,
      \e[94m"servlet-class"\e[0m: \e[32m"org.cofax.cds.CDSServlet"\e[0m,
      \e[94m"init-param"\e[0m: {
        \e[94m"configGlossary:installationAt"\e[0m: \e[32m"Philadelphia, PA"\e[0m,
        \e[94m"configGlossary:adminEmail"\e[0m: \e[32m"ksm@pobox.com"\e[0m,
        \e[94m"configGlossary:poweredBy"\e[0m: \e[32m"Cofax"\e[0m,
        \e[94m"configGlossary:poweredByIcon"\e[0m: \e[32m"/images/cofax.gif"\e[0m,
        \e[94m"configGlossary:staticPath"\e[0m: \e[32m"/content/static"\e[0m,
        \e[94m"templateProcessorClass"\e[0m: \e[32m"org.cofax.WysiwygTemplate"\e[0m,
        \e[94m"templateLoaderClass"\e[0m: \e[32m"org.cofax.FilesTemplateLoader"\e[0m,
        \e[94m"templatePath"\e[0m: \e[32m"templates"\e[0m,
        \e[94m"templateOverridePath"\e[0m: \e[32m""\e[0m,
        \e[94m"defaultListTemplate"\e[0m: \e[32m"listTemplate.htm"\e[0m,
        \e[94m"defaultFileTemplate"\e[0m: \e[32m"articleTemplate.htm"\e[0m,
        \e[94m"useJSP"\e[0m: false,
        \e[94m"jspListTemplate"\e[0m: \e[32m"listTemplate.jsp"\e[0m,
        \e[94m"jspFileTemplate"\e[0m: \e[32m"articleTemplate.jsp"\e[0m,
        \e[94m"cachePackageTagsTrack"\e[0m: 200,
        \e[94m"cachePackageTagsStore"\e[0m: 200,
        \e[94m"cachePackageTagsRefresh"\e[0m: 60,
        \e[94m"cacheTemplatesTrack"\e[0m: 100,
        \e[94m"cacheTemplatesStore"\e[0m: 50,
        \e[94m"cacheTemplatesRefresh"\e[0m: 15,
        \e[94m"cachePagesTrack"\e[0m: 200,
        \e[94m"cachePagesStore"\e[0m: 100,
        \e[94m"cachePagesRefresh"\e[0m: 10,
        \e[94m"cachePagesDirtyRead"\e[0m: 10,
        \e[94m"searchEngineListTemplate"\e[0m: \e[32m"forSearchEnginesList.htm"\e[0m,
        \e[94m"searchEngineFileTemplate"\e[0m: \e[32m"forSearchEngines.htm"\e[0m,
        \e[94m"searchEngineRobotsDb"\e[0m: \e[32m"WEB-INF/robots.db"\e[0m,
        \e[94m"useDataStore"\e[0m: true,
        \e[94m"dataStoreClass"\e[0m: \e[32m"org.cofax.SqlDataStore"\e[0m,
        \e[94m"redirectionClass"\e[0m: \e[32m"org.cofax.SqlRedirection"\e[0m,
        \e[94m"dataStoreName"\e[0m: \e[32m"cofax"\e[0m,
        \e[94m"dataStoreDriver"\e[0m: \e[32m"com.microsoft.jdbc.sqlserver.SQLServerDriver"\e[0m,
        \e[94m"dataStoreUrl"\e[0m: \e[32m"jdbc:microsoft:sqlserver://LOCALHOST:1433;DatabaseName=goon"\e[0m,
        \e[94m"dataStoreUser"\e[0m: \e[32m"sa"\e[0m,
        \e[94m"dataStorePassword"\e[0m: \e[32m"dataStoreTestQuery"\e[0m,
        \e[94m"dataStoreTestQuery"\e[0m: \e[32m"SET NOCOUNT ON;select test='test';"\e[0m,
        \e[94m"dataStoreLogFile"\e[0m: \e[32m"/usr/local/tomcat/logs/datastore.log"\e[0m,
        \e[94m"dataStoreInitConns"\e[0m: 10,
        \e[94m"dataStoreMaxConns"\e[0m: 100,
        \e[94m"dataStoreConnUsageLimit"\e[0m: 100,
        \e[94m"dataStoreLogLevel"\e[0m: \e[32m"debug"\e[0m,
        \e[94m"maxUrlLength"\e[0m: 500,
        \e[94m"log"\e[0m: \e[90mnull\e[0m}},
    {
      \e[94m"servlet-name"\e[0m: \e[32m"cofaxEmail"\e[0m,
      \e[94m"servlet-class"\e[0m: \e[32m"org.cofax.cds.EmailServlet"\e[0m,
      \e[94m"init-param"\e[0m: {
      \e[94m"mailHost"\e[0m: \e[32m"mail1"\e[0m,
      \e[94m"mailHostOverride"\e[0m: \e[32m"mail2"\e[0m}},
    {
      \e[94m"servlet-name"\e[0m: \e[32m"cofaxAdmin"\e[0m,
      \e[94m"servlet-class"\e[0m: \e[32m"org.cofax.cds.AdminServlet"\e[0m},

    {
      \e[94m"servlet-name"\e[0m: \e[32m"fileServlet"\e[0m,
      \e[94m"servlet-class"\e[0m: \e[32m"org.cofax.cds.FileServlet"\e[0m},
    {
      \e[94m"servlet-name"\e[0m: \e[32m"cofaxTools"\e[0m,
      \e[94m"servlet-class"\e[0m: \e[32m"org.cofax.cms.CofaxToolsServlet"\e[0m,
      \e[94m"init-param"\e[0m: {
        \e[94m"templatePath"\e[0m: \e[32m"toolstemplates/"\e[0m,
        \e[94m"log"\e[0m: 1,
        \e[94m"logLocation"\e[0m: \e[32m"/usr/local/tomcat/logs/CofaxTools.log"\e[0m,
        \e[94m"logMaxSize"\e[0m: \e[32m""\e[0m,
        \e[94m"dataLog"\e[0m: 1,
        \e[94m"dataLogLocation"\e[0m: \e[32m"/usr/local/tomcat/logs/dataLog.log"\e[0m,
        \e[94m"dataLogMaxSize"\e[0m: \e[32m""\e[0m,
        \e[94m"removePageCache"\e[0m: \e[32m"/content/admin/remove?cache=pages&id="\e[0m,
        \e[94m"removeTemplateCache"\e[0m: \e[32m"/content/admin/remove?cache=templates&id="\e[0m,
        \e[94m"fileTransferFolder"\e[0m: \e[32m"/usr/local/tomcat/webapps/content/fileTransferFolder"\e[0m,
        \e[94m"lookInContext"\e[0m: 1,
        \e[94m"adminGroupID"\e[0m: 4,
        \e[94m"betaServer"\e[0m: true}}],
  \e[94m"servlet-mapping"\e[0m: {
    \e[94m"cofaxCDS"\e[0m: \e[32m"/"\e[0m,
    \e[94m"cofaxEmail"\e[0m: \e[32m"/cofaxutil/aemail/*"\e[0m,
    \e[94m"cofaxAdmin"\e[0m: \e[32m"/admin/*"\e[0m,
    \e[94m"fileServlet"\e[0m: \e[32m"/static/*"\e[0m,
    \e[94m"cofaxTools"\e[0m: \e[32m"/tools/*"\e[0m},

  \e[94m"taglib"\e[0m: {
    \e[94m"taglib-uri"\e[0m: \e[32m"cofax.tld"\e[0m,
    \e[94m"taglib-location"\e[0m: \e[32m"/WEB-INF/tlds/cofax.tld"\e[0m}}}
    EOS
  end
end
